import _ from 'lodash';
import Registry from './Registry.json';
import { isMobile } from 'react-device-detect';

const load = async (props) => {
    const componentPath = chooseComponent(props);
    return componentPath ? (await import(`${componentPath}`)).default : null;
}

const chooseComponent = (props) => {
    if (props.type === 'section') {
        return './components/Section';
    } else if (props.platform && props.platform === 'mobile' && !isMobile) {
        return null;
    } else {
        const component = _.find(Registry, (rule) => {
            const typeMatch = rule.type === props.type;
            const levelMatch = props.level ? rule.level === props.level : true;

            return typeMatch && levelMatch;
        });

        if (!component) {
            // return fallback
            return './components/BasicText';
        } 

        return component.path;
    }
}

export default load;