import '../index.css';

import React from 'react';

class FileUpload extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.changeHandler(this.props.id, event.target.value);
    }

    render() {
        return (
            <div>
                 <label>{this.props.question}<input type="file" onChange={this.handleChange} /></label>
            </div>
        );
    }
}

export default FileUpload;