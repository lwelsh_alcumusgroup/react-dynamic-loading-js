import '../index.css';

import React from 'react';
import DatePicker from "react-datepicker";
import moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css';

class Date extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: moment(this.props.state[this.props.id])
        };
    }

    handleChange = (date) => {
        this.props.changeHandler(this.props.id, date);
        this.setState({
            startDate: date
        });

    };

    render() {
        return (
            <div>
                <label>{this.props.question}<DatePicker selected={this.state.startDate} onChange={this.handleChange} /></label>
            </div>
        );
    }
}

export default Date;