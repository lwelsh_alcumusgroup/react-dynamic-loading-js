import React from 'react';

class Textarea extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.changeHandler(this.props.id, event.target.value);
    }

    render() {
        return (
            <div>
                <label>{this.props.question}<textarea value={this.props.state[this.props.id]} onChange={this.handleChange}/></label>
            </div>
        );
    }
}

export default Textarea;