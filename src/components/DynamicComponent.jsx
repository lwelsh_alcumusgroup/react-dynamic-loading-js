import '../index.css';

import React from 'react';
import load from '../LoadComponents';


class DynamicComponent extends React.Component {
    component;
    
    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        this.component = await load(this.props);
        this.setState({ loaded: true});
    }

    render() {
        if (!this.component) {
            // components are not suitable (ie - wrong platform)
            return <div></div>;
        } else {
            return this.renderComponent()
        }
    }

    renderComponent = () => {
        const Component = this.component;

        return <Component {...this.props} />
    };
}

export default DynamicComponent;