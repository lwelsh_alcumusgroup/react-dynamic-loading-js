import '../index.css';

import React from 'react';

class Fancynumber extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.changeHandler(this.props.id, event.target.value);
    }

    render() {
        return (
            <div>
                 <label>{this.props.question}<input type="number" style={{backgroundColor:'pink'}} step="5" value={this.props.state[this.props.id]} onChange={this.handleChange} /></label>
            </div>
        );
    }
}

export default Fancynumber;