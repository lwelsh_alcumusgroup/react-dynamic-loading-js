import React from 'react';

class CommaList extends React.Component {

    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.changeHandler(this.props.id, event.target.value);
    };

    render() {
        const css =` .comma-separated:after{content: ",";}  .comma-separated-wrap .comma-separated:last-child:after{visibility:hidden;}`;
        return (
            <div className="comma-separated-wrap">
                <style>
                    {css}
                </style>
                <p>{this.props.question}</p>
                {this.props.list.map((value, key) => {
                    return (<div className="comma-separated" key={key}>
                                <label>{value.value}
                                    <input type="text" placeholder={value.text} onChange={this.handleChange} />
                                </label>
                            </div>)
                })}
            </div>
        );
    }
}

export default CommaList;