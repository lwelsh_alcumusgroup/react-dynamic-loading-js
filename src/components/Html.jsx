import '../index.css';

import React from 'react';

class Html extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div dangerouslySetInnerHTML={{__html: this.props.content}}></div>
        );
    }
}

export default Html;