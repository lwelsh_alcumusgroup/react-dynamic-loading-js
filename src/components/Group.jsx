import '../index.css';

import React from 'react';
import _ from 'lodash';

import DynamicComponent from './DynamicComponent';

class Group extends React.Component {
    component;
    
    constructor(props) {
        super(props);
    }

    borderStyle = {
        border: '1px solid black'
    };

    render() {
        return (
            
            <div style={this.borderStyle}>
                <p dangerouslySetInnerHTML={{__html: this.props.question}}></p>
                {
                    this.props.group.map((question, key) => {
                        return  <DynamicComponent state={this.props.state} changeHandler={this.props.changeHandler} {...question} key={key}/> 
                    })
                }
               
            </div>
        );
    }
}

export default Group;