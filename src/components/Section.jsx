import '../index.css';

import React from 'react';
import _ from 'lodash';

import DynamicComponent from './DynamicComponent';

class Section extends React.Component {
    component;
    
    constructor(props) {
        super(props);
    }

    render() {
        return (
            
            <div>
                <h2 dangerouslySetInnerHTML={{__html: this.props.title}}></h2>
                <p dangerouslySetInnerHTML={{__html: this.props.description}}></p>
                {
                    this.props.questions.map((question, key) => {
                        return  <DynamicComponent state={this.props.state} changeHandler={this.props.changeHandler} {...question} key={key}/> 
                    })
                }
               
            </div>
        );
    }
}

export default Section;