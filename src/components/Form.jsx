import '../index.css';

import React from 'react';
import DynamicComponent from './DynamicComponent';

class Form extends React.Component {
    state;

    constructor(props) {
        super(props);
    }

    componentWillMount = () => {
       const constructState = {};
       console.log(this.props);
        const sections = this.props.data.sections;
        
        // TODO - refactor
        sections.forEach((section) => {
            section.questions.forEach((question) => {
                constructState[question.id] = question.default || '';
            });
        });

		this.setState(constructState);
    }

    render() {
        return (
             <form onSubmit={this.handleSubmit}>
                <h1 dangerouslySetInnerHTML={{__html: this.props.data.heading.title}}></h1>
                <p dangerouslySetInnerHTML={{__html: this.props.data.heading.description}}></p>
                {
                    this.props.data.sections.map((section, id) => {
                        return this.renderSection(section, id);
                    })
                }
                <p dangerouslySetInnerHTML={{__html: this.props.data.footer.contents}}></p>
                <input type="submit" value="Submit" />
            </form>
        );
    }

    handleSubmit = (event) => {
        console.log(this.state);
        event.preventDefault();
    }

    handleChange = (id, value) => {
        this.setState({ [id]: value });
    }

    renderSection = (section, id) => {
        return <DynamicComponent {...section} state={this.state} changeHandler={this.handleChange} key={id}/>
    }
}

export default Form;