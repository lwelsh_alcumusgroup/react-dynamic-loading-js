import '../index.css';

import React from 'react';


class Bool extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <fieldset style={{border:'none'}}>
                    <legend>{this.props.question}</legend>
                    <label><input type="radio" name={this.props.name} value="true" defaultChecked={this.props.default === true}/>Yes</label><br/>
                    <label><input type="radio" name={this.props.name} value="false" defaultChecked={this.props.default === false}/>No</label>
                </fieldset>
            </div>
        );
    }
}

export default Bool;