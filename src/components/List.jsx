import '../index.css';

import React from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

class List extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.changeHandler(this.props.id, event.target.value);
    }

    render() {
        return (
            <div>
                <Dropdown options={this.props.list} onChange={this.handleChange} value={this.props.default} placeholder={this.props.question}/>
            </div>
        );
    }
}

export default List;