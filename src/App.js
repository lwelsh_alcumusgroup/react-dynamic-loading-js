import React, { Component } from 'react';
import './App.css';

// components
import Form from './components/Form';

// config json
import Config from './form.json';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="App">
                {this.renderForm()}
            </div>
        );
    }

    renderForm = () => {
        return <Form data={Config}/>
    }
}

export default App;
